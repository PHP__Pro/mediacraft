# README

This project is PHP CMS (XIBO) Project.

##Image:

![dashboard](image/my_project.png)

## Install

    1. Set Document Root Path in Apache conf file (C:\xampp\apache\conf\httpd.conf)
        ex: DocumentRoot "D:/mediacraft/web"
            <Directory "D:/mediacraft/web"> 
    2. Create MySQL DB in phpMyAdmin. (dbname, username, password)
    3. Install DB using sql (Import in phpMyAdmin)
    
        