<?php
/*
 * Xibo - Digital Signage - http://www.xibo.org.uk
 * Copyright (C) 2006-2015 Daniel Garner
 *
 * This file is part of Xibo.
 *
 * Xibo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Xibo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Xibo.  If not, see <http://www.gnu.org/licenses/>.
 */


$config = array(
    'theme_name' => 'Media Craft London',
    'theme_title' => 'Media Craft London',
    'app_name' => 'MCManagement',
    'theme_url' => 'http://mediacraftlondon.co.uk',
    'cms_source_url' => 'http://mediacraftlondon.co.uk',
    'cms_install_url' => 'http://mediacraftlondon.co.uk',
    'cms_release_notes_url' => 'http://mediacraftlondon.co.uk'
);
